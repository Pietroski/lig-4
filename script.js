let victory = false;
let player = "black";

let grid = document.querySelector("#grid");

let tabuleiro = [];

function createGrid(column, row) {
    victory = false;
    player = "black";

    document.querySelector(".jogador").className = player + " jogador";

    grid.innerHTML = '';
    
    for (let x = 0; x < column; x++) {
        const white_space = document.createElement('div');
        white_space.className = 'wp';
        let column = document.createElement("div");
        column.className = "column";
        column.value = x;
        grid.appendChild(column);

        
        for (let y = 0; y < row; y++) {
            let cell = document.createElement("div");
            cell.className = "cell";
            column.appendChild(cell);
        }
    }

    for(let x = 0; x < row; x++){
        tabuleiro[x] = [];
        for(let y = 0; y < column; y++){
            tabuleiro[x][y] = 0;
        }
    }

    const col = document.querySelectorAll('.column');

    for (let i = 0; i < col.length; i++) {
        col[i].addEventListener('click', criaDiscos);
    }

    let variaveis = [];
    for (let i = 0; i < 7; i++) {
        variaveis[i] = 0;
    }

    let player_red = [];
    let player_black = [];

    function criaDiscos(e) {
        if(!victory){
            let numero = e.currentTarget.value;

            if (e.currentTarget.childElementCount < 7 && variaveis[numero] < 6) {
                const disc = document.createElement('div');
                disc.setAttribute('class', `disc ${player}`);
                e.currentTarget.appendChild(disc);
        
                let element = col[numero].lastElementChild;
        
                col[numero].childNodes[variaveis[numero]].appendChild(element);
                animationDisc(disc, numero, variaveis[numero]);
                variaveis[numero]++;
                
                (player == 'black') ? (tabuleiro[variaveis[numero]-1][numero] = 1) : (tabuleiro[variaveis[numero]-1][numero] = 2)

                testaVitoria();

                if (player == 'black') {
                    player = 'red';
                } else if (player == 'red') {
                    player = 'black';
                }

                document.querySelector(".jogador").className = player + " jogador";
            }
        }
    }
}

createGrid(7, 6);

function animationDisc(disc, columnIndex, rowIndex){
    const rowPosition = [2, 56, 110, 164, 218, 272].reverse();

    let column = grid.children[columnIndex];

    column.style.position = "relative";

    disc.style.position = "absolute";
    
    disc.style.top = "2px";

    let animation = setInterval(() => {
        if(disc.style.top !== rowPosition[rowIndex] + "px"){
            let top = disc.style.top;
            top = Number(top.split("px")[0]);
            top += 2;
            // console.log(top);
            disc.style.top = top + "px";
        }else{
            clearInterval(animation);
        }
    }, 1);
}

function vitoria(){
    if(!victory){
        victory = true;

        let vitoria = document.querySelector("#vitoria");
        vitoria.style.margin = "1rem";

        let reinicia = document.createElement("button");
        reinicia.textContent = "Reiniciar";
        //reinicia.className = player;
        reinicia.addEventListener('click', () => {
            vitoria.innerHTML = '';
            vitoria.style.margin = 0;
            createGrid(7, 6);
        });

        if(player !== "empate"){
            vitoria.innerHTML = `Parabéns Player ${player}! Você VENCEU!  <br>`;
        }else{
            vitoria.innerHTML = `Puts... Deu EMPATE! Não perca as esperanças! Tente Novamente! <br>`;
        }
        vitoria.appendChild(reinicia);
    }
}

function testaVitoria(){
    // HORIZONTAL OK
    for(let x = 0; x < tabuleiro.length; x++){
        for(let y = 0; y < tabuleiro[x].length - 3; y++){
            if(tabuleiro[x][y] !== 0){
                if(tabuleiro[x][y] === tabuleiro[x][y+1] &&
                    tabuleiro[x][y+1] === tabuleiro[x][y+2] &&
                    tabuleiro[x][y+2] === tabuleiro[x][y+3]){
                        vitoria();
                }
            }
        }
    } 

    // VERTICAL OK
    for(let x = 0; x < tabuleiro.length - 3; x++){
        for(let y = 0; y < tabuleiro[x].length; y++){
            if(tabuleiro[x][y] !== 0){
                if(tabuleiro[x][y] === tabuleiro[x+1][y] &&
                    tabuleiro[x+1][y] === tabuleiro[x+2][y] &&
                    tabuleiro[x+2][y] === tabuleiro[x+3][y]){
                        vitoria();
                }
            }
        }
    }

    // DIAGONAL OK
    for(let x = 0; x < tabuleiro.length - 3; x++){
        for(let y = 0; y < tabuleiro[x].length - 3; y++){
            if(tabuleiro[x][y] !== 0){
                if(tabuleiro[x][y] === tabuleiro[x+1][y+1] &&
                    tabuleiro[x+1][y+1] === tabuleiro[x+2][y+2] &&
                    tabuleiro[x+2][y+2] === tabuleiro[x+3][y+3]){
                        vitoria();
                }
            }
        }
    }

    // DIAGONAL OK
    for(let x = 0; x < tabuleiro.length - 3; x++){
        for(let y = tabuleiro[x].length - 1; y >= 3; y--){
            if(tabuleiro[x][y] !== 0){
                if(tabuleiro[x][y] === tabuleiro[x+1][y-1] &&
                    tabuleiro[x+1][y-1] === tabuleiro[x+2][y-2] &&
                    tabuleiro[x+2][y-2] === tabuleiro[x+3][y-3]){
                        vitoria();
                }
            }
        }
    }

    // EMPATE OK
    if(tabuleiro[tabuleiro.length - 1].indexOf(0) === -1){
        //console.log("EMPATE");
        player = "empate";
        vitoria();
    }
}